import cv2
from flask import Flask, render_template_string, Response

from .gui_app import GuiApp
from .. import types as t
from ..utils import info


app = Flask(__name__)


class WebApp(GuiApp):
    '''
    App without a GUI but continue to detect tiles and communicate with the hexacode api.
    '''

    HTML_CONTENT = '''<body>
<div class="container">
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <h3 class="mt-5">Live Streaming</h3>
            <img src="{{ url_for('video_feed') }}" width="100%">
        </div>
    </div>
</div>
</body>'''

    def __init__(self, config: t.Config):
        super().__init__(config)
        self.image: t.Image

    def start(self) -> None:
        info('starting app in headless mode')
        app.run(debug=True)

    def render(self) -> None:
        pass

    def get_frames(self):
        _, buffer = cv2.imencode('.jpg', self.image)
        frame = buffer.tobytes()
        yield (b'--frame\r\n'
                b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

    @app.route('/')
    def index(self):
        return render_template_string(self.HTML_CONTENT)

    @app.route('/video_feed')
    def video_feed(self):
        return Response(self.get_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')
