from .app import App
from .. import types as t
from ..utils import info


# pylint: disable=import-outside-toplevel
def from_config(config: t.Config) -> App:
    info('loading the app based on user config')

    if config.gui.headless:
        from .headless_app import HeadlessApp
        return HeadlessApp(config)

    if config.gui.web:
        from .web_app import WebApp
        return WebApp(config)

    from .window_app import WindowApp
    return WindowApp(config)
