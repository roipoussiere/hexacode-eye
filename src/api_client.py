import json

import requests

from . import types as t
from .utils import warn


class ApiClient:
    '''
    Handle all requests with hexacode API
    '''

    def __init__(self, api_config: t.Config):
        self.api_config = api_config
        self.api_url = api_config.api_url
        self.headers = { 'content-type': 'application/json' }

    def is_activated(self) -> bool:
        if self.api_config.disable_api:
            return False

        try:
            response = requests.get('%s/status' % self.api_url, headers=self.headers)
            response.raise_for_status()
            return True
        except requests.exceptions.ConnectionError:
            warn('Can not connect to server %s: network error.\n' % self.api_url \
                + 'Is the url correct and service accessible?')
        except requests.exceptions.RequestException as error:
            warn('Can not connect to server %s:\n%s' % (self.api_url, str(error)))

        return False

    def update_map(self, hex_map: dict) -> None:
        payload = { 'map': hex_map }

        try:
            response = requests.put('%s/map' % self.api_url,
                                    data=json.dumps(payload), headers=self.headers)
            response.raise_for_status()
        except requests.exceptions.RequestException as error:
            warn('Can not connect to server %s:\n%s' % (self.api_url, str(error)))
