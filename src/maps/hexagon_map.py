import math

from ..utils import info
from .. import types as t
from .map import Map


class HexagonalMap(Map):
    '''
    An hexagonal map.
    '''

    SLOT_KEYS = 'abcdefghijklmnopqrstuvwxyz'
    ASPECT_RATIO = 2 / math.sqrt(3)

    def __init__(self, map_config: t.Config, input_size: t.ImgSize, nb_slots_edge: int):
        info('initializing hexagonal map with %d slots in edge, sized at %dx%d' \
             % (nb_slots_edge, *input_size) )
        super().__init__(map_config, input_size)
        self.nb_slots_edge = nb_slots_edge
        self.nb_slots_diag = nb_slots_edge * 2 - 1
        self.fit_on_width = self.ASPECT_RATIO > self.size[0] / self.size[1]
        self._inner_size = (0, 0)
        self._offset = (0, 0)
        self._segment_length = 0.0

    @property
    def inner_size(self) -> t.ImgSize:
        if self._inner_size != (0, 0):
            return self._inner_size

        width, height = self.size
        if self.fit_on_width:
            height = round(width / self.ASPECT_RATIO)
        else:
            width = round(height * self.ASPECT_RATIO)

        width -= self.slot_radius * 2
        height -= self.slot_radius * 2

        self._inner_size = (width, height)
        info('map inner size: %dx%d' % self._inner_size)
        return self._inner_size

    @property
    def segment_length(self) -> float:
        if self._segment_length == 0.0:
            self._segment_length = self.inner_size[0] / (self.nb_slots_diag - 1)
            info('segment length: %dpx / %d slots = %.2f' \
                % (self.inner_size[0], self.nb_slots_diag - 1, self._segment_length))
        return self._segment_length

    @property
    def offset(self) -> t.Point:
        if self._offset != (0, 0):
            return self._offset

        try:
            pos_x, pos_y = [ int(n) for n in self.map_config.map_offset.split('x') ]
        except ValueError as err:
            raise SystemExit('Error: bad map offset: `%s`.' % self.map_config.map_offset) from err

        if self.fit_on_width:
            pos_x += self.slot_radius
            pos_y += round(self.size[1] / 2 - self.inner_size[1] / 2)
        else:
            pos_x += round(self.size[0] / 2 - self.inner_size[0] / 2)
            pos_y += self.slot_radius

        self._offset = (pos_x, pos_y)
        return self._offset

    @property
    def lattice(self) -> t.Lattice:
        '''
        Returns the map lattice, which is a dictionnary of slots, where:
        - key is a string representing a slot identifier (see `get_slots_dict()`);
        - value is a Point, giving the position of the slot in the input image.
        '''

        if self._lattice != {}:
            return self._lattice

        info('generating map lattice')
        height_length = self.segment_length / self.ASPECT_RATIO

        for slot_id, slot in self.get_slots_dict().items():
            slots_offset_x = abs(self.nb_slots_edge / 2 - (slot[1] + 1) * 0.5)
            pos_x = int( self.segment_length * (slot[0] + slots_offset_x) + self.offset[0] )
            pos_y = int( height_length * slot[1] + self.offset[1] )
            self._lattice[slot_id] = pos_x, pos_y

        return self._lattice

    def get_slots_dict(self) -> t.Lattice:
        '''
        Returns a dictionary of slots, where:
        - key is a string representing a slot identifier;
        - value is a Point, giving the position in a 2d-array repensenting the hexagon.
        Example for nb_slots_edge == 2:
             . .         { 'aa': (0, 0), 'ba': (1, 0),
            . . .   ->     'ab': (0, 1), 'bb': (1, 1), 'cb': (2, 1),
             . .           'ac': (0, 2), 'bc': (1, 2) }
        '''

        slots_dict = {}
        for row in range(self.nb_slots_diag):
            offset = row if row < self.nb_slots_edge else (self.nb_slots_diag - row - 1)
            row_length = self.nb_slots_edge + offset
            for col in range(row_length):
                slots_dict['%s%s' % (self.SLOT_KEYS[col], self.SLOT_KEYS[row])] = (col, row)

        return slots_dict
