from __future__ import annotations
from abc import abstractmethod

from ..utils import info
from .. import types as t


class Map:
    '''
    Abstract class used to manage maps.
    Mainly used to process their lattice, which is a group of slots.
    Maps are sized according to the input source size.
    '''

    def __init__(self, map_config: t.Config, size: t.ImgSize):
        self.map_config = map_config
        self.size = size
        self._lattice: t.Lattice = {}
        self._slot_radius = 0

    @property
    @abstractmethod
    def lattice(self) -> t.Lattice:
        pass

    @property
    def slot_radius(self) -> int:
        if self._slot_radius == 0:
            self._slot_radius = round(self.map_config.slot_size / 2 / 100 * self.size[0])
            info('slot radius: %s%% of %dpx ~= %dpx' \
                % (self.map_config.slot_size, self.size[0], self._slot_radius) )
        return self._slot_radius
