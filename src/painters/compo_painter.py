import numpy as np
import cv2

from .map_painter import MapPainter
from .marker_painter import MarkerPainter
from .overlay_painter import OverlayPainter
from .img_utils import ImgUtils
from .. import types as t
from ..maps.map import Map
from ..marker import Marker


# pylint: disable=too-many-instance-attributes
class CompoPainter:
    '''A painter class used to compose the output image'''

    def __init__(self, compo_config: t.Config, tile_map: Map, marker_type: str):
        self.compo_config = compo_config

        marker = Marker.from_label(marker_type)
        scale = self.compo_config.image_width / tile_map.size[0] \
                if self.compo_config.image_width > 1 else self.compo_config.image_width

        self.img_size = int(tile_map.size[0] * scale), int(tile_map.size[1] * scale)
        nb_images = 1 if self.compo_config.hide_map or self.compo_config.hide_capture else 2
        self.size = self.img_size[0], nb_images * self.img_size[1]

        self.marker_painter = MarkerPainter(marker, tile_map.slot_radius * 3)
        self.map_painter = MapPainter(tile_map, scale)
        self.overlay_painter = OverlayPainter(tile_map, scale, self.compo_config.overlay)

        self.img_stream: t.Image
        self.image: t.Image

    def load(self):
        if '-' not in self.compo_config.overlay:
            self.overlay_painter.load(self.marker_painter)

        if not self.compo_config.hide_map:
            self.map_painter.load()

    def draw_stream(self, capture_image: t.Image, fps: float) -> None:
        if self.compo_config.hide_capture:
            return

        self.overlay_painter.status_bar_text = 'FPS: %d' % fps
        self.img_stream = cv2.resize(capture_image, self.img_size)

    def compose_images(self) -> None:
        if self.compo_config.hide_capture and self.compo_config.hide_map:
            raise SystemExit('Error: capture and map are both hidden, can not display anything.')

        if self.compo_config.hide_capture:
            self.image = self.map_painter.image
        else:
            self.image = self.img_stream

            if '-' not in self.compo_config.overlay:
                ImgUtils.overlay(self.image, self.overlay_painter.image)

            if not self.compo_config.hide_map:
                self.image = np.concatenate((self.image, self.map_painter.image), axis=0)
