from abc import abstractmethod
from typing import Dict

import cv2

from .. import types as t
from ..utils import warn
from ..maps.map import Map
from ..tile_finder import TileFinder
from .img_utils import ImgUtils


class Painter:
    '''
    Abstract class to draw various shapes on an image.
    '''

    LINE_STYLE = cv2.LINE_AA
    LINE_STYLE = cv2.LINE_AA
    LINE_THICKNESS = 1
    FONT = cv2.FONT_HERSHEY_PLAIN
    FONT_SCALE = 1

    def __init__(self, tile_map: Map, scale: float):
        self.map = tile_map
        self._scale = scale

        self.size = self.scale(self.map.size)

        self.background = ImgUtils.new_image(self.size)
        self.image = ImgUtils.new_image(self.size)
        self.sprites: Dict[int, t.Image] = {}

    def scale(self, point: t.Point):
        return int(point[0] * self._scale), int(point[1] * self._scale)

    def load(self, *args) -> None:
        self.draw_background() # TODO: new image for 3th time
        self.image = self.background.copy() # TODO: and 4th
        self.load_sprites(*args)

    @abstractmethod
    def load_sprites(self, *args) -> None:
        pass

    @abstractmethod
    def draw_background(self) -> None:
        pass

    @abstractmethod
    def draw(self, tile_finder: TileFinder) -> None:
        pass

    def draw_sprite(self, marker: t.Marker) -> None:
        marker_id, marker_pos = marker
        try:
            sprite_image = self.sprites[marker_id]
        except KeyError:
            warn('marker %d detected but not found in tiles dictionary' % marker_id)
            return

        offset = int(sprite_image.shape[0] / 2)
        marker_pos = self.scale( (marker_pos[0] - offset, marker_pos[1] - offset) )

        ImgUtils.offset_overlay(self.image, sprite_image, marker_pos)

    def draw_sprites(self, markers: Dict[str, t.Marker]) -> None:
        for slot_id, marker in markers.items():
            marker = marker[0], self.map.lattice[slot_id]
            self.draw_sprite(marker)
