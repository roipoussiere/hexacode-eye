import os.path as op

import cv2

from ..painters.img_utils import ImgUtils
from ..marker import Marker
from .. import types as t
from .. import utils


class MarkerPainter:
    '''
    Painter class used to draw markers.
    '''

    def __init__(self, marker_type: Marker, sprite_width: int):
        utils.info('initializing marker painter')
        self.marker_type = marker_type
        self.sprite_width = sprite_width

        self.mosaic_path = op.join(utils.MARKERS_PATH, self.marker_type.label,
                                   'generated', 'mosaic.png')

    def get_atomic_sprite(self, marker_id: int) -> t.Image:
        if marker_id < 0 or marker_id >= self.marker_type.amount:
            raise SystemExit('Error: marker id %d is not defined ' % marker_id +
                'in the %s set: ' % self.marker_type.name +
                'it must be a number between 0 and %d.' % (self.marker_type.amount - 1))

        marker_size = (self.marker_type.size, self.marker_type.size)
        bg_img = ImgUtils.new_image( marker_size, '#FFF')

        mosaic_img = cv2.imread(self.mosaic_path, flags=cv2.IMREAD_UNCHANGED)

        img_width = mosaic_img.shape[1]
        nb_cols = int( (img_width + 1) / (self.marker_type.size + 1) )

        pos_x = marker_id % nb_cols * (self.marker_type.size + 1)
        pos_y = marker_id // nb_cols * (self.marker_type.size + 1)

        marker_img = ImgUtils.crop(mosaic_img, (pos_x, pos_y), marker_size)
        ImgUtils.overlay(bg_img, marker_img)
        return bg_img

    def get_sprite(self, marker_id: int):
        marker_img = self.get_atomic_sprite(marker_id)
        size = (self.sprite_width, self.sprite_width)
        return cv2.resize(marker_img, size, interpolation=cv2.INTER_NEAREST)
