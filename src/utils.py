import os.path as op
import sys


PROJECT_PATH = op.dirname(op.dirname(op.abspath(__file__)))

MARKERS_PATH = op.join(PROJECT_PATH, 'markers')

SPRITES_PATH = op.join(PROJECT_PATH, 'sprites')
ATLAS_IMAGE_PATH = op.join(SPRITES_PATH, 'atlas.png')
ATLAS_INDEX_PATH = op.join(SPRITES_PATH, 'atlas.txt')

CONFIG_DEFAULT_PATH = op.join(PROJECT_PATH, 'config.yml')
DEFAULT_CONFIG_PATH = op.join(PROJECT_PATH, 'config.dist.yml')


def info(text: str) -> None:
    if '-v' in sys.argv or '--verbose' in sys.argv:
        print(text)

def warn(text: str) -> None:
    sys.stderr.write('Warning: %s\n' % text)
