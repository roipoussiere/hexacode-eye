# Resources:
# https://docs.opencv.org/4.5.4/d8/dfe/classcv_1_1VideoCapture.html

import cv2

from .capture import Capture
from .camera_info import CameraInfo
from ..utils import info
from .. import types as t


class WebcamCapture(Capture):
    '''
    Input source dedicated to get webcam stream.
    '''

    def __init__(self, input_config: t.Config):
        info('initialising webcam')
        super().__init__(input_config)

        self.device_info = CameraInfo(input_config)
        self.webcam: cv2.VideoCapture
        info('using %s as input source' % self.name)

    def get_name(self) -> str:
        return self.device_info.name

    def get_size(self) -> t.ImgSize:
        return self.device_info.nearest_size

    def start(self) -> None:
        info('starting webcam %s' % self.device_info.path)
        self.webcam = cv2.VideoCapture(self.input_config.device)
        self.webcam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*self.input_config.video_codec))
        self.webcam.set(cv2.CAP_PROP_FRAME_WIDTH, self.size[0])
        self.webcam.set(cv2.CAP_PROP_FRAME_HEIGHT, self.size[1])

    def shoot(self) -> None:
        success, image = self.webcam.read()
        if not success:
            raise SystemExit('Error: can not take picture with %s.' % self.name)

        if self.input_config.mirror:
            image = cv2.flip(image, 1)

        self.image = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
