# Resources:
# https://picamera.readthedocs.io/en/release-1.13/

from time import sleep

#from picamera.array import PiRGBArray
from picamera import PiCamera # pylint: disable=import-error
import numpy as np

from .capture import Capture
from ..utils import info
from .. import types as t


class PiCamCapture(Capture):
    '''
    Input source dedicated to get Raspberry Pi camera stream.
    '''

    CAPTURE_FORMAT = 'bgra'

    def __init__(self, input_config: t.Config):
        info('initialising picam')
        super().__init__(input_config)

        self.pi_camera: PiCamera
        info('using %s as input source' % self.name)

    def get_name(self) -> str:
        return 'Raspberry Pi camera'

    def get_size(self) -> t.ImgSize:
        return (320, 240)

    def start(self) -> None:
        info('starting picam')
        self.image = np.empty((240 * 320 * 4,), dtype=np.uint8)

        self.pi_camera = PiCamera()
        self.pi_camera.resolution = self.size
        self.pi_camera.framerate = 32

        sleep(2)

    def shoot(self) -> None:
        self.pi_camera.capture(self.image, self.CAPTURE_FORMAT)
        self.image = self.image.reshape((240, 320, 4))
