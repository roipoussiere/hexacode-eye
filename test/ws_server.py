# https://github.com/Pithikos/python-websocket-server

# See also https://docs.godotengine.org/fr/stable/tutorials/networking/websocket.html
# and https://docs.godotengine.org/en/latest/tutorials/networking/websocket.html#minimal-server-example
# for a websocket server example implemented in Godot


import logging
from websocket_server import WebsocketServer


SERVER_HOST = '127.0.0.1'
SERVER_PORT = 8000


def new_client(client, server):
    ip, port = client['address']
    print('%s:%d: new client' % (ip, port))

    msg = 'Hello, client %s:%d! I\'m the server.' % (ip, port)
    print('sending response `%s`' % msg)
    server.send_message_to_all(msg)

def message_received(client, server, message):
    ip, port = client['address']
    print('%s:%d: message received: %s' % (ip, port, message))

def client_left(client, server):
    ip, port = client['address']
    print('%s:%d: client left' % (ip, port))

server = WebsocketServer(SERVER_PORT, host=SERVER_HOST, loglevel=logging.INFO)

server.set_fn_new_client(new_client)
server.set_fn_message_received(message_received)
server.set_fn_client_left(client_left)
server.run_forever()
