# Hexacode eye

The **Hexacode** project is a visual programming language based on hexagons.

This **hexacode-eye** repository is dedicated to tiles detection on webcam pictures of a physical hexacode map.

## Basic usage

Find markers from webcam:

```
./src/hexacode-eye.py
```

Find markers from image:

```
./src/hexacode-eye.py <marker_image.jpg>
```

Example:

```
./src/hexacode-eye.py ./markers/TagCircle21h4/pictures/circle21h4_test-page_2.jpg
```

## Installation

### 0. Setup Raspberry Pi

If you plan to install Hexacode on a Raspberry Pi, you may want to follow the following step, otherwise skip to step 1.

#### Requirements

- Raspberry Pi (any model can be used, but newer models will be faster for board recognition);
- micro SD card (at least 8GB);
- micro USB power supply (at least 2.4A);
- Raspberry Pi camera module or a USB webcam;
- a computer (the *host*);
- or a micro-SD card reader (or just a micro SD adapter if your computer includes a SD card reader)

#### Install Raspberry Pi OS

On your host:

1. download Raspberry Pi Imager: `sudo apt install rpi-imager` (or see [the official guide](https://www.raspberrypi.com/software/));
2. plug the micro-SD card in the reader;
3. run Raspberry Pi Imager, then:
    - **Operating system**: select *Raspberry Pi OS (other)*, then *Raspberry Pi OS Lite (32 bits)* (a desktop environment is not necessary to run Hexacode);
    - **Storage**: select the SD card;
    - **Options**: type `ctrl+shift+X`, then:
        - Enable SSH: chose *allow public key authentication only*;
        - Configure wifi: set SSID and password (required if you don't use a network cable);
        - Set locale settings: set time zone (optional);
4. connect the camera module or the USB webcam, the SD card, and the power supply.

> If you missed the *Options* part when installing your Pi, or if you want to use an already installed OS, you can configure it using a monitor and keyboard:
>
> On your Pi, run `sudo raspi-config`, then:
> - enable SSH: select `3. Interface options` / `P2. SSH` / `yes`;
> - configure wifi: `1. System options` / `S1. Wireless LAN`

#### Configure connexion to you Pi

Always on your host:

1. check the Pi IP adress: `arp -a`
2. edit your host file (`sudo nano /etc/hosts`) and add your Pi. For instance: `192.168.1.14    hexacode`.
3. check if ssh keys are generated: `ls ~/.ssh`;
4. if not, generate them: `ssh-keygen -t rsa` (you can leave default path (`~/.ssh/id_rsa.pub`), passphrase is not required);
5. install the public key on the Pi: `ssh-copy-id -i ~/.ssh/id_rsa.pub pi@hexacode`;
6. ensure key-based SSH is working: `ssh pi@hexacode`;

The next steps will take place in this ssh connexion.

> If you did not selected *allow public key authentication only* in the *Pi Imager* options: you should do it now for security reasons:
>
> - via the Pi ssh conection, edit the sshd config file: `sudo nano /etc/ssh/sshd_config`;
> - add the following lines at the end:
>
> ```
> PermitRootLogin no
> PasswordAuthentication no
> ChallengeResponseAuthentication no
> UsePAM no
> ```
>
> - then reload the SSH daemon so changes can take effect: `sudo systemctl reload sshd`.

#### Enable and check the Pi camera

On your Pi:

1. if you use a Pi camera module (not a USB camera), enable it:
    - `sudo raspi-config`
    - `3. Interface options` / `P1. Camera` / `yes`;
2. install vlc and v4l-utils: `sudo apt install vlc v4l-utils`;
3. optional: check your camera resolution: `v4l2-ctl --list-framesizes=H264`
4. capture a video stream via and share it over the network via vlc: `raspivid --timeout 0 --nopreview --width 800 --height 600 --output - | cvlc stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8090}' :demux=h264` (errors may appear, but try the next step anyway).

Now, on your host:

- display the video stream via vlc: `cvlc http://192.168.1.19:8090`

You can take the opportunity to check your hardware setup (distance from camera to board, etc.).

### 1. Install system requirements

```
sudo apt install cmake git python3
```

### 2. Get Hexacode eye

```
git clone https://framagit.org/hexacode/hexacode-eye.git
cd hexacode-eye
```

### 3. Install Python dependencies

```
python3 -m venv .venv
.venv/bin/pip install -r requirements.txt
```

If you plan to use the Raspberry Pi camera, install PiCamera module as well:

```
.venv/bin/pip install "picamera[array]"
```

### 4. Install AprilTag

[AprilTag](https://april.eecs.umich.edu/software/apriltag) is a visual fiducial system popular for robotics research. It is used in the Hexacode project in order to identify tiles.

#### 4.1. Download AprilTag sources

```
mkdir vendors
git clone https://github.com/AprilRobotics/apriltag.git vendors/apriltag
```

#### 4.2. Add the custom marker type source file

Hexacode uses the custom AprilTag marker type `TagCircle21h4`, chosen to its circular shape, small size (9 pixels) and amount of unique markers (1762).

All required files related to this marker type have already been generated in `markers/TagCircle21h4/generated`, so you just have to copy these files to AprilTag sources:

```
cp markers/tagCircle21h4/generated/tagCircle21h4.* vendors/apriltag
```

#### 4.3. Edit AprilTag sources

The source file of the custom marker type is added, but not yet visible in the AprilTag Python library.

You must edit the library sources:

```
nano vendors/apriltag/apriltag_pywrap.c
```

1. add `#include "tagCircle21h4.h"` in the include section, just before `#include "tagCircle21h7.h"`;
2. add `    _(tagCircle21h4)                        \` in the define section, just before `    _(tagCircle21h7)                        \`.

#### 4.4. Compile AprilTag

```
cd vendors/apriltag
cmake .
sudo make install
cd ../..
```

#### 4.5. Install AprilTag Python module

cp vendors/apriltag/apriltag.cpython-3*.so .venv/lib/python3.*/site-packages

#### 4.6. Set library path

AprilTag library requires that the environment variable `LD_LIBRARY_PATH` must be set to `/usr/local/lib` *before* calling the Python executable. You could call the script with `LD_LIBRARY_PATH=/usr/local/lib python ./find_tiles.py`, but it's annoying.

The followig lines replaces the Python executable in your virtual env by a small script that set the environment variable, then call the Python executable.

```
mv .venv/bin/python .venv/bin/python.old
echo -e '#!/bin/sh\nexport LD_LIBRARY_PATH="/usr/local/lib"\n"$0".old "$@"' > .venv/bin/python
chmod u+x .venv/bin/python
```

#### 4.7. Check AprilTag installation

```
.venv/bin/python -c "from apriltag import apriltag; apriltag('tagCircle21h4'); print('Ok')"
```

### 5. Build sprites

```
cd ..
git clone https://framagit.org/hexacode/hexacode-sprites.git
cd hexacode-sprites
python3 -m venv .venv
.venv/bin/pip install -r requirements.txt
./src/main.py --width 50
cd ../hexacode-eye
ln -s ../hexacode-sprites/sprites sprites
```

### 6. Edit configuration

Now copy the sample config file and edit the modified version accoding to your camera device and your needs (`config.dist.yml` is self-documented):

```
cp config.dist.yml config.yml
nano config.yml
```

---

At this point, Hexacode Eye should be fully installed and working. You can check by running `./hexacode-eye`.

Working? Congratulations! You may want to check the manual in the next section.

## Manual

The following documentation is based on `./src/hexacode-eye.py --help`.

Note that these options (except `-h`, `-C` and `-v`) can also be set in the configration file.

### optional arguments

- `-h, --help`: show this help message and exit

### Global

- `-C CONFIG, --config CONFIG`: path of a file to load the config from
- `-v, --verbose`: enable verbose mode

### GUI

- `-H, --headless`: disable the GUI, but still communicate marker positions via the api
- `-f, --fullscreen`: open window in fullscreen
- `-w IMAGE_WIDTH, --image-width IMAGE_WIDTH`: width of rendered images (in px)
- `-s IMAGE_SCALE, --image-scale IMAGE_SCALE`: reduction ratio to apply to rendered images, ignoring image width

### Composition

- `-c, --hide-capture`: don't display video capture
- `-m, --hide-map`: don't display generated map
- `--horizontal` compose images horizontally instead vertically
- `-o OVERLAY, --overlay OVERLAY`: list of elements that should appear on overlay image
(g=grid, l=links, p=good polygons, f=too far polygons, n=not squared polygons, -=disable overlay)

### API

- `--disable-api`: disable communication with hexacode api
- `-u API_URL, --api-url API_URL`: url of the hexacode api

### Capture

- `-i IMAGE_PATH, --image-path IMAGE_PATH`: path of an image to analyse instead a webcam stream
- `-d DEVICE_ID, --device-id DEVICE_ID`: video device identifier (0, 1, ...)
- `-r VIDEO_RESOLUTION, --video-resolution VIDEO_RESOLUTION`: requested video capture resolution (with <x>x<y> notation, in px)
- `--video-codec VIDEO_CODEC`: four-character code of the codec used to decode video (ie. MJPG or JPEG)
- `--mirror`: mirror input image
- `--flip FLIP`: flip input image - one of: [ 0 (0°), 1 (90°), 2 (180°), 3 (-90°) ]

### Detection

- `--max-marker-size-diff MAX_MARKER_SIZE_DIFF`: max difference between expected slot size and marker size (in px)
- `--max-marker-distance MAX_MARKER_DISTANCE`: max distance between expected slot position and marker position (in px)
- `--detect-each DETECT_EACH`: number of frames to read between two detections (O to disable auto-detect)
- `--marker-type MARKER_TYPE`: marker type (see readme)

### Map

- `--map-type MAP_TYPE`: type of map used to associate a tile position to a slot
- `--map-offset MAP_OFFSET`: offset applied to the map position (with <x>x<y> notation, in px)
- `--map-scale MAP_SCALE`: scale ratio between capture image and map
- `--slot-size SLOT_SIZE`: slot size (in % of image width)

## Contributing

Thank you! Please check the [contribution guide](./CONTRIBUTING.md).

## Contact, authorship and license

- Contact: Mail: roipoussiere ⓐ protonmail.com | Mastodon: roipoussiere ⓐ mastodon.tetaneutral.net
- Credits: Nathanaël Jourdane and contributors
- License: [MIT](./LICENSE).
